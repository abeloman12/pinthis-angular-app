/**
 *
 * You can write your JS code here, DO NOT touch the default style file
 * because it will make it harder for you to update.
 *
 */

"use strict";

$(document).ready(function () {
    var autosizable_txt_area = $('.text_area');
    autosizable_txt_area.trigger('autosize.resize');
});

// file chooser style 
let fileButton = $('#actualFileBtn');
let decoy = $('#fileLabel');
let choosenFileDisplay = $('#fileDisplay');

// src list 
var srcs = [];
var current_index = 0;
$('.ai-items').each(function () {
    var src = $(this).attr('src');
    srcs.push(src);
});

decoy.on('click', function () {
    fileButton.click();
});

fileButton.on('change', function (event) {
    var files = event.target.files;

    if (files.length > 0) {
        $("#pre1").css('display', 'none');
        $("#pre2").css('display', 'none');
        $("#pre3").css('display', 'none');


        $.each(files, (i, val) => {
            var imgurl = URL.createObjectURL(val);
            var img = '<img width="50" height="50" src="' + imgurl + '" class="ai-items ai-dync">';

            $('.all-images').append(img);
        });

        $('.preview-image').attr("src", URL.createObjectURL(event.target.files[0]));

        srcs = [];
        $('.ai-dync').each(function () {
            var src = $(this).attr('src');
            srcs.push(src);
        });
    }
});

$('#swap').click(function () {
    $('.preview-image').attr('src', srcs[current_index]);
    current_index += 1;
    if (current_index == srcs.length) {
        current_index = 0;
    }

});

$(document).on('click', '.ai-items', function (e) {
    var img = e.currentTarget;
    var src = $(img).attr('src');
    $('.preview-image').attr('src', src);

});
