var chart = new Chartist.Line('.ct-chart', {
    labels: ['Feb 1', 'Mar 3', 'Mar 9', 'Mar 27', 'Apr 11'],
    series: [
        [6, 9, 7, 8, 5]
    ]
}, {
    fullWidth: true,
    showArea: true,
    chartPadding: {
        right: 30
    },
    axisY: {
        onlyInteger: true,
        offset: 20
    }
});

chart.on('draw', function (data) {
    if (data.type === 'line' || data.type === 'area') {
        data.element.animate({
            d: {
                begin: 2000 * data.index,
                dur: 2000,
                from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                to: data.path.clone().stringify(),
                easing: Chartist.Svg.Easing.easeOutQuint
            }
        });
    }
});

var chart2 = new Chartist.Bar('.ct-chart2', {
    labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
    series: [
        [5, 4, 3, 7, 5, 10, 3]
    ]
}, {
    reverseData: true,
    horizontalBars: true,
    axisY: {
        offset: 35,
        onlyInteger: true
    },
    axisX: {
        onlyInteger: true
    }
});
