import { BaseService } from './../../shared/services/base.service';
import { BadInputError, UnauthorizedError, NotFoundError, AppError } from './../../shared/errors/app-error';
import { AdminStoreService, api_url } from './../../shared/services/admin-store.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private options;
  private url:string;

  constructor(private baseService: BaseService) { 
    this.url=api_url+"categories/";
    let headers = new Headers();

    this.options = baseService.prepareRequestHeader();
  }

  public getAll(): Observable<any> {
    return this.baseService.get(this.url).pipe(map((response:any) => {
      return response;
    }))
  }

  public getCategory(category_id): Observable<any> {
    return this.baseService.get(this.url + category_id).pipe(map((response:any) => {
      return response;
    }))
  }

  public getAd(): Observable<any> {
    return this.baseService.get(this.url + 'ad').pipe(map((response:any) => {
      return response;
    }))
  }

  public addCategory(form:any){
    console.log(this.url);
    console.log(form);
    return this.baseService.post(this.url, form, this.options)
    .pipe(map((response:any) => {
      return response;
    }), retry(1), catchError(this.handleError));
  }

  public editCategory(form:any, id:string){
    return this.baseService.put(this.url + id, form, this.options)
    .pipe(map((response:any) => {
      return response;
    }), retry(1), catchError(this.handleError));
  }

  private handleError(error: Response) {
    console.log("HANDLED");
    console.log(error);
    if (error.status === 400) {
      return Observable.throw(new BadInputError(error));
    }
    else if (error.status === 401) {
      return Observable.throw(new UnauthorizedError());
    }
    else if (error.status === 404) {
      return Observable.throw(new NotFoundError());
    }
    else {
      return Observable.throw(new AppError(error));
    }
  }
}
