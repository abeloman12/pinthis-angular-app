import { DashboardService } from './dashboard.service';
import { CustomerService } from './customer.service';
import { SubadminService } from './subadmin.service';
import { CategoryService } from './category.service';
import { SubcategoryService } from './subcategory.service';
import { PostService } from './post.service';
import { SuperAdminGuardService } from './super-admin-guard.service';
export const superAdminInjectable: Array<any> = [
    {provide:CategoryService,useClass:CategoryService},
    {provide:SubcategoryService,useClass:SubcategoryService},
    {provide:SubadminService,useClass:SubadminService},
    {provide:CustomerService,useClass:CustomerService},
    {provide:DashboardService,useClass:DashboardService},
    {provide:PostService,useClass:PostService},
    {provide:SuperAdminGuardService,useClass:SuperAdminGuardService}
];