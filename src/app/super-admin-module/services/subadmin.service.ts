import { BaseService } from './../../shared/services/base.service';
import { api_url } from './../../shared/services/admin-store.service';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SubadminService {

  private url:string;

  constructor(
    private baseService: BaseService
  ) { 
    this.url=api_url+"subcategory_admins/";
  }

  public getAll(): Observable<any> {
    return this.baseService.get(this.url).pipe(map((response:any) => {
      return response;
    }))
  }

  public getOnGoing(): Observable<any> {
    return this.baseService.get(this.url + 'on_going').pipe(map((response:any) => {
      return response;
    }))
  }

  public getTerminated(): Observable<any> {
    return this.baseService.get(this.url + 'terminated').pipe(map((response:any) => {
      return response;
    }))
  }

  public getSubAdmin(subadmin_id): Observable<any> {
    return this.baseService.get(this.url + subadmin_id).pipe(map((response:any) => {
      return response;
    }))
  }

  public verifySubAdmin(subadmin_id): Observable<any> {
    return this.baseService.post(this.url + "verify_subcategoryAdmin/" + subadmin_id, {}).pipe(map((response:any) => {
      console.log(response);
      return response;
    }))
  }

  public setTermination(subadmin_id, value): Observable<any> {
    return this.baseService.post(this.url + "set_terminate/" + subadmin_id, {value: value}).pipe(map((response:any) => {
      console.log(response);
      return response;
    }))
  }
}
