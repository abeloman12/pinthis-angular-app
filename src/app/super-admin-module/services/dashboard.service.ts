import { Observable } from 'rxjs';
import { api_url } from './../../shared/services/admin-store.service';
import { BaseService } from './../../shared/services/base.service';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(
    private baseService: BaseService
  ) { }

  getAllCustomers(): Observable<any> {
    return this.baseService.get(api_url + "customers/").pipe(map((response: any) => {
      return response;
    }))
  }

  public getAllCategories(): Observable<any> {
    return this.baseService.get(api_url + "categories/").pipe(map((response:any) => {
      return response;
    }))
  }

  public getAllPosts(): Observable<any> {
    return this.baseService.get(api_url + "posts/").pipe(map((response:any) => {
      return response;
    }))
  }

  public getAllSubCategories(): Observable<any> {
    return this.baseService.get(api_url + "subcategories/").pipe(map((response:any) => {
      return response;
    }))
  }

  public getNumberofCustomerPerYear(): Observable<any> {
    return this.baseService.get(api_url + "customer/count_per_year").pipe(map((response:any) => {
      return response;
    }))
  }

  public getAdmin(user_id): Observable<any> {
    return this.baseService.get(api_url + "admins/user/" + user_id).pipe(map((response:any) => {
      return response;
    }))
  }

  public updateAdmin(content: any) {
    console.log("IN SERVICE");
    return this.baseService.put(api_url + "admins/", content).pipe(
      map((response) => {
        return response;
      }));
  }
}
