import { map } from 'rxjs/operators';
import { api_url } from './../../shared/services/admin-store.service';
import { Observable } from 'rxjs';
import { BaseService } from './../../shared/services/base.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  private url:string;

  constructor(
    private baseService: BaseService
  ) { 
    this.url=api_url+"posts/";
  }

  public getAll(): Observable<any> {
    return this.baseService.get(this.url).pipe(map((response:any) => {
      return response;
    }))
  }

  public removePost(post_id): Observable<any> {
    return this.baseService.delete(this.url + post_id).pipe(map((response:any) => {
      return response;
    }))
  }
}
