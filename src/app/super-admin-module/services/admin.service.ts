import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { BadInputError, UnauthorizedError, NotFoundError, AppError } from 'src/app/shared/errors/app-error';
import { throwError } from 'rxjs';
import { catchError, retry, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private url = "http://localhost:3000/v1/admins";

  constructor(private httpClient: HttpClient) { }

  public getAdminFromUser(user_id) {
    let options = this.prepareRequestHeader();

    return this.httpClient.get(this.url + '/user/' + user_id, options).pipe(
      map((response) => {
        return response;
      }),
      retry(1),
      catchError(this.handleError)
    );
  }

  public changePassword(content: any) {
    console.log("IN SERVICEee");
    let options = this.prepareRequestHeader();

    return this.httpClient.post(this.url + '/changePassword', content, options).pipe(
      map((response) => {
        return response;
      }),
      retry(1),
      catchError(this.handleError)
    );
  }

  private prepareRequestHeader() {
    let token = localStorage.getItem('token');
    return {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token
      })
    };
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 400) {
      return throwError(new BadInputError(error));
    }
    else if (error.status === 401) {
      return throwError(new UnauthorizedError(error));
    }
    else if (error.status === 404) {
      return throwError(new NotFoundError(error));
    }
    return throwError(new AppError(error));
  }
}
