import { Observable } from 'rxjs';
import { BaseService } from './../../shared/services/base.service';
import { api_url } from './../../shared/services/admin-store.service';
import { Injectable } from '@angular/core';
import { map, retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SubcategoryService {

  private url:string;

  constructor(private baseService: BaseService) { 
    this.url=api_url+"subcategories/";
  }

  public getAll(category_id): Observable<any> {
    return this.baseService.get(this.url + 'category/' + category_id).pipe(map((response:any) => {
      return response;
    }))
  }

  public getAds(): Observable<any> {
    return this.baseService.get(this.url + '/super_admin').pipe(map((response:any) => {
      return response;
    }))
  }

  public addAd(ad): Observable<any> {
    return this.baseService.post(this.url, ad).pipe(map((response: any) => {
      return response;
    }))
  }

  public removeSub(sub_id): Observable<any> {
    return this.baseService.delete(this.url + sub_id).pipe(map((response:any) => {
      return response;
    }))
  }
}
