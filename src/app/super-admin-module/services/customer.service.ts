import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { api_url } from './../../shared/services/admin-store.service';
import { BaseService } from './../../shared/services/base.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private options;
  private url:string;

  constructor(
    private baseService: BaseService
  ) { 
    this.url=api_url+"customers/";
    this.options = baseService.prepareRequestHeader();
  }



  public getAll(): Observable<any> {
    return this.baseService.get(this.url).pipe(map((response:any) => {
      return response;
    }))
  }
}
