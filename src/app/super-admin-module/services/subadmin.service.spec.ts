import { TestBed } from '@angular/core/testing';

import { SubadminService } from './subadmin.service';

describe('SubadminService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SubadminService = TestBed.get(SubadminService);
    expect(service).toBeTruthy();
  });
});
