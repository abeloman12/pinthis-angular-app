import { AdminStoreService } from './../../shared/services/admin-store.service';
import { AuthService } from './../../main/services/auth.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SuperAdminGuardService {

  constructor(
    private auth_svc:AuthService,
    private storage_svc:AdminStoreService,
    private router:Router) { }
    canActivate() {
      if (this.auth_svc.isLoggedIn()) {
        return true;
      }
      else {
        this.router.navigate(['/login']);
        return false;
      }
    }
}
