import { AdListComponent } from './components/ad/ad-list/ad-list.component';
import { DataTablesModule } from 'angular-datatables';
import { AdminFooterComponent } from './components/admin-template/admin-footer/admin-footer.component';
import { AdminSidebarComponent } from './components/admin-template/admin-sidebar/admin-sidebar.component';
import { AdminNavbarComponent } from './components/admin-template/admin-navbar/admin-navbar.component';
import { AdminPageComponent } from './components/admin-template/admin-page/admin-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './../shared/shared.module';
// import { AppRoutingModule } from './../app-routing.module';
// import { Ng2SearchPipeModule } from 'ng2-search-filter';
// import { Ng2OrderModule } from 'ng2-order-pipe';
// import { NgxPaginationModule } from 'ngx-pagination';
import { superAdminInjectable } from './services/super-admin-injectable';
import { FileUploadModule } from 'ng2-file-upload';
import { BrowserModule } from '@angular/platform-browser';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ChartModule } from 'primeng/chart';
import { MessageService } from 'primeng/components/common/messageservice';
import { ToastModule } from 'primeng/toast';

// import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { SuperAdminHeaderComponent } from './components/super-admin-template/super-admin-header/super-admin-header.component';
// import { SuperAdminFooterComponent } from './components/super-admin-template/super-admin-footer/super-admin-footer.component';
// import { SuperAdminPageComponent } from './components/super-admin-template/super-admin-page/super-admin-page.component';
import { SuperAdminDashboardComponent } from './components/super-admin-dashboard/super-admin-dashboard.component';
import { SuperAdminCategoryListComponent } from './components/super-admin-categories/super-admin-category-list/super-admin-category-list.component';
import { CategoryAddComponent } from './components/super-admin-categories/category-add/category-add.component';
import { CategoryEditComponent } from './components/super-admin-categories/category-edit/category-edit.component';
import { SubcategoryListComponent } from './components/super-admin-subcategories/subcategory-list/subcategory-list.component';
import { SubcategoryAdminListComponent } from './components/subcategory-admin/subcategory-admin-list/subcategory-admin-list.component';
import { SubcategoryAdminDetailComponent } from './components/subcategory-admin/subcategory-admin-detail/subcategory-admin-detail.component';
import { CustomerListComponent } from './components/customer/customer-list/customer-list.component';
import { PostListComponent } from './components/post/post-list/post-list.component';
import { SuperAdminGuardService } from './services/super-admin-guard.service';
import { SuperAdminProfileComponent } from './components/profile/super-admin-profile/super-admin-profile.component';
import { AdAddComponent } from './components/ad/ad-add/ad-add.component';
import { TerminatedSubcategoryAdminComponent } from './components/terminated-subcategory-admin/terminated-subcategory-admin.component';
// import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    // SuperAdminHeaderComponent,
    // SuperAdminFooterComponent,
    // SuperAdminPageComponent,
    AdListComponent,
    AdminPageComponent,
    AdminNavbarComponent,
    AdminSidebarComponent,
    AdminFooterComponent,
    SuperAdminDashboardComponent,
    SuperAdminCategoryListComponent,
    CategoryAddComponent,
    CategoryEditComponent,
    SubcategoryListComponent,
    SubcategoryAdminListComponent,
    SubcategoryAdminDetailComponent,
    CustomerListComponent,
    PostListComponent,
    SuperAdminProfileComponent,
    AdAddComponent,
    TerminatedSubcategoryAdminComponent
  ],
  imports: [
    MessageModule,
    MessagesModule,
    ToastModule,
    ChartModule,
    FileUploadModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    BrowserModule,
    RouterModule.forRoot([
      {
        path: "super-admin",
        component: AdminPageComponent,
        canActivate: [SuperAdminGuardService],
        children: [
          {
            path: "",
            redirectTo: "dashboard",
            pathMatch: "full"
          },
          {
            path: "dashboard",
            component: SuperAdminDashboardComponent
          },
          {
            path: "profile",
            component: SuperAdminProfileComponent
          },
          {
            path:"ad/list",
            component: AdListComponent
          },
          {
            path:"category/list",
            component: SuperAdminCategoryListComponent
          },
          {
            path:"customer/list",
            component: CustomerListComponent
          },
          {
            path:"post/list",
            component: PostListComponent
          },
          {
            path:"subadmin/list",
            component: SubcategoryAdminListComponent
          },
          {
            path:"subadmin/list/terminated",
            component: TerminatedSubcategoryAdminComponent
          },
          {
            path:"subadmin/detail",
            component: SubcategoryAdminDetailComponent
          },
          // {
          //   path:"subadmin/:id",
          //   component: SubcategoryAdminDetailComponent
          // },
          {
            path: "ad/add",
            component: AdAddComponent
          },
          {
            path:"category/add",
            component: CategoryAddComponent
          },
          {
            path:"category/edit",
            component: CategoryEditComponent
          },
          {
            path:"category/:id",
            component: SubcategoryListComponent
          }
        ]
      }

    ]) 
    // AppRoutingModule
  ],
  providers: [
    superAdminInjectable,
    MessageService,
  ],
  // bootstrap: [AppComponent]
})
export class SuperAdminModule { }
