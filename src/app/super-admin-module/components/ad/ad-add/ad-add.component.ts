import { MessageService } from 'primeng/components/common/messageservice';
import { UnauthorizedError } from 'src/app/shared/errors/app-error';
import { BadInputError, NotFoundError } from './../../../../shared/errors/app-error';
import { FileUploader } from 'ng2-file-upload';
import { api_url } from 'src/app/shared/services/admin-store.service';
import { CategoryService } from './../../../../super-admin-module/services/category.service';
import { Message } from 'primeng/components/common/message';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { SubcategoryService } from 'src/app/super-admin-module/services/subcategory.service';

@Component({
  selector: 'app-ad-add',
  templateUrl: './ad-add.component.html',
  styleUrls: ['./ad-add.component.css']
})
export class AdAddComponent implements OnInit {
  url;
  uploader;
  itemForm: FormGroup;
  // uploadedImages=[];
  uploadedImage;
  imageURL;
  a;
  // imgs = [];
  img;
  colorOptions = [];
  categories;
  selectedCategory;
  f;
  itemFormMessages: Message[];
  msgs: Message[] = [];
  toasts:Message[] = [];
  removeImage = false;
  imageUploaded = false;
  avail_colors = []

  constructor(
    public sanitization:DomSanitizer,
    private category_svc: CategoryService,
    private subcategory_svc:SubcategoryService,
    private fb:FormBuilder,
    private router:Router,
    private messageService: MessageService,
  ) { 
    this.itemForm = this.fb.group({
      name:['',Validators.required],
      image_paths:[''],
      description:['', Validators.required]
    })
    this.f = this.itemForm.controls;
  }

  ngOnInit() {
    this.url=api_url+"upload/";
    this.uploader=new FileUploader({url:this.url,itemAlias:"fileData"})
    
    this.uploader.onAfterAddingFile=(file)=>{
          this.hideWarn()

      file.withCredentials=false;
      this.imageURL  = this.sanitization.bypassSecurityTrustUrl((window.URL.createObjectURL(file._file)));
      // this.imgs.push(this.imageURL);
      this.img = this.imageURL;
    };

    this.uploader.onCompleteItem=(item:any,response:any,status:any,headers:any)=>{
      
      this.uploadedImage = response;

      this.category_svc.getAd().subscribe(res => {
        let category_id = res._id;
        // console.log(category_id);
  
        this.itemForm.value.image = this.uploadedImage;
        this.itemForm.value.category_id = category_id;
        this.itemForm.value.by_superadmin = true;
  
        console.log(this.itemForm.value);
  
        this.subcategory_svc.addAd(this.itemForm.value).subscribe(resp => {
          this.router.navigate(["super-admin", "ad", "list"]);
        },(error)=>{
          if (error instanceof BadInputError) {
    
            this.messageService.add(
              {
                severity: 'error',
                summary: 'Ad Create Failed',
                detail: error.originalError.error.errors
              }
            )
          }
          else if (error instanceof UnauthorizedError) {
            this.router.navigate(['login']);
          }
          else if (error instanceof NotFoundError) {
            this.router.navigate(['404']);
          }
          else {
            console.log(error);
            throw error;
          }
        })
      })

    }
    this.showWarn()
  }

  showWarn() {
    this.msgs = [];
    this.msgs.push({severity:'warn', summary:'Image Required', detail:'Ad Image is required'});
  }

  hideWarn(){
    this.imageUploaded = true
    this.msgs = []
  }

  addAd(){
    this.uploader.uploadAll();  
  }
}
