import { AppError } from 'src/app/shared/errors/app-error';
import { MessageService } from 'primeng/components/common/messageservice';
import { Subject } from 'rxjs';
import { AdminStoreService } from './../../../../shared/services/admin-store.service';
import { Component, OnInit } from '@angular/core';
import { SubcategoryService } from 'src/app/super-admin-module/services/subcategory.service';

@Component({
  selector: 'app-ad-list',
  templateUrl: './ad-list.component.html',
  styleUrls: ['./ad-list.component.css']
})
export class AdListComponent implements OnInit {
  private subcategoryList: any[] = [];
  public dtTrigger: Subject<any> = new Subject();
  private category: any = null;

  constructor(
    private admin_store: AdminStoreService,
    private subcategory_svc: SubcategoryService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
      this.subcategory_svc.getAds()
        .subscribe((response) => {
          this.subcategoryList = response;
          this.dtTrigger.next();
        });
  }

  ngOnDestroy() {
    this.dtTrigger.unsubscribe();
  }

  removeAd(subcategory) {
    console.log("CLIKC");
    this.subcategory_svc.removeSub(subcategory._id)
    .subscribe((response) => {
      this.messageService.add({
        severity: 'success',
        summary: 'Successful',
        detail: 'Ad removed successfully!'
      });

      this.subcategory_svc.getAds()
      .subscribe((response) => {
        this.subcategoryList = response;
        // this.dtTrigger.next();
      });
    }, (error: AppError) => {
      this.messageService.add({
        severity: 'error',
        summary: 'Error Occurred',
        detail: 'Error Removing Ad!'
      });
    });
  }

}
