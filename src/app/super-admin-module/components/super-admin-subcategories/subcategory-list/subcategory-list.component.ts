import { CategoryService } from './../../../services/category.service';
import { AdminStoreService } from './../../../../shared/services/admin-store.service';
import { Component, OnInit } from '@angular/core';
import { SubcategoryService } from 'src/app/super-admin-module/services/subcategory.service';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-subcategory-list',
  templateUrl: './subcategory-list.component.html',
  styleUrls: ['./subcategory-list.component.css']
})
export class SubcategoryListComponent implements OnInit {

  private subcategoryList: any[] = [];
  public dtTrigger: Subject<any> = new Subject();
  private category: any = null;
  // key: String = 'created_at';
  filter;
  constructor(
    private activatedRoute: ActivatedRoute,
    private admin_store: AdminStoreService,
    private subcategory_svc: SubcategoryService,
    private category_svc: CategoryService
    // private router:Router,
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap
    .subscribe((params) => {
      this.subcategory_svc.getAll(params.get('id'))
        .subscribe((response) => {
          this.subcategoryList = response;
          this.dtTrigger.next();
        });

      this.category_svc.getCategory(params.get('id'))
        .subscribe((res) => {
          this.category = res;
        })
    });
  }

  ngOnDestroy() {
    this.dtTrigger.unsubscribe();
  }

  editCategory(category){
    // this.admin_store.storeItem("category",category)
    // this.router.navigate(['super-admin','category','edit'])
    // this.router.navigate(['super-admin', 'category', 'edit', category._id]);
  }
  
  selectSubcategory(subCategory) {
    // console.log(subCategory);
    // this.router.navigate(['super-admin', 'category', category._id]);
  }

}
