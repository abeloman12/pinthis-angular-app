import { DashboardService } from './../../../services/dashboard.service';
import { AdminStoreService } from './../../../../shared/services/admin-store.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/main/services/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PasswordConfirmationValidator } from 'src/app/shared/validators/password-confirmation.validators';
import { emailAvailabilityValidator } from 'src/app/shared/validators/email-availability.validators';
import { ValidatorService } from 'src/app/shared/services/validator.service';
import { AppError, UnauthorizedError, NotFoundError, BadInputError } from 'src/app/shared/errors/app-error';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/components/common/messageservice';
import { Message } from 'primeng/components/common/message';
import { AdminService } from 'src/app/super-admin-module/services/admin.service';

@Component({
  selector: 'app-super-admin-profile',
  templateUrl: './super-admin-profile.component.html',
  styleUrls: ['./super-admin-profile.component.css']
})
export class SuperAdminProfileComponent implements OnInit {
  private admin: any;

  public updateProfileForm: FormGroup = new FormGroup({
    first_name: new FormControl('', Validators.required),
    last_name: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    gender: new FormControl('', Validators.required),
  });

  public changePasswordForm: FormGroup = new FormGroup({
    current_password: new FormControl('', Validators.required),
    password: new FormControl('', [Validators.required, Validators.minLength(8)]),
    confirm_password: new FormControl('', [Validators.required, Validators.minLength(8)]),
  }, PasswordConfirmationValidator.passwordConfirmationValidator);

  public updateProfileMessages: Message[] = [];
  public changePasswordMessages: Message[] = [];

  constructor(
    private dashboard_service: DashboardService,
    private admin_store: AdminStoreService,
    private router: Router,
    private authService: AuthService,
    private validatorService: ValidatorService,
    private adminService: AdminService
  ) { }

  ngOnInit() {
    console.log("From Init");
    // this.admin = this.admin_store.getUser();
    this.adminService.getAdminFromUser(this.authService.getCurrentUser()._id)
    .subscribe((response) => {
      this.admin = response;
      this.prepareUpdateForm();
    }, (error: AppError) => {
      if (error instanceof UnauthorizedError) {
        this.router.navigate(['login']);
      }
      else if (error instanceof NotFoundError) {
        this.router.navigate(['404']);
      }
      else {
        throw error;
      }
    })

    // this.adminService.getAdminFromUser(this.authService.getCurrentUser()._id)
    //   .subscribe((response) => {
    //     this.admin = response;
    //     this.prepareUpdateForm();
    //   }, (error: AppError) => {
    //     if (error instanceof UnauthorizedError) {
    //       this.router.navigate(['login']);
    //     }
    //     else if (error instanceof NotFoundError) {
    //       this.router.navigate(['404']);
    //     }
    //     else {
    //       throw error;
    //     }
    //   })
  }

  onUdateProfileSubmit() {
      let values = this.updateProfileForm.value;
      values.user_id = this.admin.user_id._id;
      this.dashboard_service.updateAdmin(values)
        .subscribe((response) => {
          this.updateProfileMessages = [];
          this.updateProfileMessages.push({
            severity: 'success',
            summary: 'Profile Update Successful',
            detail: 'Your profile has been updated successfully!'
          })
        }, (error: AppError) => {
          this.updateProfileMessages = [];
          this.updateProfileMessages.push({
            severity: 'error',
            summary: 'Profile Update Error',
            detail: error.originalError.error.errors.join(', ')
          });
        });
  }

  onChangePasswordSubmit() {
    console.log("Clicked");
    this.adminService.changePassword(this.prepareChangePasswordContent(this.changePasswordForm.value.current_password, this.changePasswordForm.value.password))
      .subscribe((response) => {
        console.log(response);
        this.changePasswordMessages = [];
        this.changePasswordMessages.push(
          {
            severity: 'success',
            summary: 'Change Password Successful',
            detail: 'Your password has been changed successfully!'
          }
        )
      }, (error: AppError) => {
        console.log("Errror)");
        if (error instanceof BadInputError) {
          this.changePasswordMessages = [];
          this.changePasswordMessages.push(
            {
              severity: 'error',
              summary: 'Change Password Failed',
              detail: error.originalError.error.errors
            }
          )
        }
        else if (error instanceof UnauthorizedError) {
          this.router.navigate(['login']);
        }
        else if (error instanceof NotFoundError) {
          this.router.navigate(['404']);
        }
        else {
          throw error;
        }
      });
  }

  private prepareUpdateForm() {
    this.first_name = this.admin.first_name;
    this.last_name = this.admin.last_name;
    this.email = this.admin.user_id.email;
    this.gender = this.admin.gender;
  }

  private prepareChangePasswordContent(currentPassword: string, password: string) {
    return {
      'user_id' : this.authService.getCurrentUser()._id,
      'currentPassword': currentPassword,
      'password': password
    }
  }

  get first_name() {
    return this.updateProfileForm.get('first_name');
  }

  set first_name(value) {
    this.updateProfileForm.get('first_name').setValue(value);
  }

  get last_name() {
    return this.updateProfileForm.get('last_name');
  }

  set last_name(value) {
    this.updateProfileForm.get('last_name').setValue(value);
  }

  get email() {
    return this.updateProfileForm.get('email');
  }

  set email(value) {
    this.updateProfileForm.get('email').setValue(value);
  }

  get gender() {
    return this.updateProfileForm.get('gender');
  }

  set gender(value) {
    this.updateProfileForm.get('gender').setValue(value);
  }

  get currentPassword() {
    return this.changePasswordForm.get('current_password');
  }

  get newPassword() {
    return this.changePasswordForm.get('password');
  }

  get confirmPassword() {
    return this.changePasswordForm.get('confirm_password');
  }

}

