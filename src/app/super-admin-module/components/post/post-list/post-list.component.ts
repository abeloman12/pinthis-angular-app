import { MessageService } from 'primeng/components/common/messageservice';
import { AppError } from 'src/app/shared/errors/app-error';
import { PostService } from './../../../services/post.service';
import { Component, OnInit } from '@angular/core';
import { api_url } from 'src/app/shared/services/admin-store.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {
  private postList: any[] = [];
  public dtTrigger: Subject<any> = new Subject();

  constructor(
    private post_svc: PostService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    this.post_svc.getAll()
      .subscribe((response) => {
        this.postList = response;
        this.dtTrigger.next();
    });
}

ngOnDestroy() {
  this.dtTrigger.unsubscribe();
}

  concat(element) {
    console.log('http://localhost:3000/' + element);
    return('http://localhost:3000/' + element);
  }

  removePost(post) {
    this.post_svc.removePost(post._id)
    .subscribe((response) => {
      this.messageService.add({
        severity: 'success',
        summary: 'Successful',
        detail: 'Post removed successfully!'
      });

      this.post_svc.getAll()
      .subscribe((response) => {
        this.postList = response;
        // this.dtTrigger.next();
      });
    }, (error: AppError) => {
      this.messageService.add({
        severity: 'error',
        summary: 'Error Occurred',
        detail: 'Error Removing Post!'
      });
    });
  }

}
