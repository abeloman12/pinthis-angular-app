import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerminatedSubcategoryAdminComponent } from './terminated-subcategory-admin.component';

describe('TerminatedSubcategoryAdminComponent', () => {
  let component: TerminatedSubcategoryAdminComponent;
  let fixture: ComponentFixture<TerminatedSubcategoryAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerminatedSubcategoryAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerminatedSubcategoryAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
