import { AppError } from 'src/app/shared/errors/app-error';
import { Subject } from 'rxjs';
import { AdminStoreService } from './../../../shared/services/admin-store.service';
import { SubadminService } from 'src/app/super-admin-module/services/subadmin.service';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/components/common/messageservice';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-terminated-subcategory-admin',
  templateUrl: './terminated-subcategory-admin.component.html',
  styleUrls: ['./terminated-subcategory-admin.component.css']
})
export class TerminatedSubcategoryAdminComponent implements OnInit {
  private subadminList: any[] = [];
  public dtTrigger: Subject<any> = new Subject();

  constructor(
    private admin_store: AdminStoreService,
    private subadmin_svc: SubadminService,
    private router:Router,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    this.subadmin_svc.getTerminated()
    .subscribe((response) => {
      this.subadminList = response;
      this.dtTrigger.next();
    });
  }

  ngOnDestroy() {
    this.dtTrigger.unsubscribe();
  }

  setTerminate(subadmin, value) {
    let boolValue = false;
    if (value == 'true') {
      boolValue = true;
    }

    this.subadmin_svc.setTermination(subadmin._id, boolValue)
    .subscribe((response) => {
      this.subadmin_svc.getTerminated()
      .subscribe((response) => {
        this.subadminList = response;
        // this.dtTrigger.next();
      });
    }, (error: AppError) => {
        this.messageService.add({
          severity: 'error',
          summary: 'Action Failed',
          detail: 'Please try again!'
        });
    });
  }

  goToDetail(subadmin){
    this.admin_store.storeItem("subAdmin",subadmin)
    this.router.navigate(['super-admin','subadmin','detail'])
  }

}
