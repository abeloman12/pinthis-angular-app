import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperAdminCategoryListComponent } from './super-admin-category-list.component';

describe('SuperAdminCategoryListComponent', () => {
  let component: SuperAdminCategoryListComponent;
  let fixture: ComponentFixture<SuperAdminCategoryListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperAdminCategoryListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperAdminCategoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
