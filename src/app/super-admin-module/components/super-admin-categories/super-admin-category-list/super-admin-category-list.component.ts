import { CategoryService } from './../../../services/category.service';
import { AdminStoreService } from './../../../../shared/services/admin-store.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-super-admin-category-list',
  templateUrl: './super-admin-category-list.component.html',
  styleUrls: ['./super-admin-category-list.component.css']
})
export class SuperAdminCategoryListComponent implements OnInit {
  private categoryList: any[] = [];
  public dtTrigger: Subject<any> = new Subject();

  constructor(
    private admin_store: AdminStoreService,
    private category_svc: CategoryService,
    private router:Router,
  ) { }

  ngOnInit() {
    this.category_svc.getAll().subscribe(res=>{
      this.categoryList = res;
      this.dtTrigger.next();
    })
  }


  ngOnDestroy() {
    this.dtTrigger.unsubscribe();
  }

  editCategory(category){
    this.admin_store.storeItem("category",category)
    this.router.navigate(['super-admin','category','edit'])
  }
  
  selectCategory(category) {
    console.log(category);
    this.router.navigate(['super-admin', 'category', category._id]);
  }

}
