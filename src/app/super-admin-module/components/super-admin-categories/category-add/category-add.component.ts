import { CategoryService } from './../../../services/category.service';
import { AppError, BadInputError } from './../../../../shared/errors/app-error';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/components/common/messageservice';

@Component({
  selector: 'app-category-add',
  templateUrl: './category-add.component.html',
  styleUrls: ['./category-add.component.css']
})
export class CategoryAddComponent implements OnInit {
  categoryForm: FormGroup;

  disableForm: boolean = true;

  constructor(
    private fb:FormBuilder,
    private messageService: MessageService,
    private category_svc: CategoryService,
    private router:Router
  ) { 

    this.categoryForm=this.fb.group({
      name:['',Validators.required],
      description:['',Validators.required]
    })
  }

  ngOnInit() {
  }

  addCategory(form:any){
      this.category_svc.addCategory(form).subscribe(res=>{
        
        this.messageService.add({
          severity: 'success',
          summary: 'Category Added',
          detail: 'Category added successfully!'
        });

        this.categoryForm.reset();
        // this.router.navigate(['super-admin','category','list']); 
      }, (error: AppError) => {
        console.log(error);
        this.messageService.add({
          severity: 'error',
          summary: 'Category Registration Failed',
          detail: 'Make sure to use valid inputs'
        });
        if (error instanceof BadInputError) {
          console.log(" Not Added");        }
        
      })  
  }

  get name() {
    return this.categoryForm.get('name');
  }

  get description() {
    return this.categoryForm.get('description');
  }

}

