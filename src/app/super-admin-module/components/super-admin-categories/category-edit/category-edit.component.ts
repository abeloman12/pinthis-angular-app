import { AdminStoreService } from './../../../../shared/services/admin-store.service';
import { BadInputError } from './../../../../shared/errors/app-error';
import { AppError } from 'src/app/shared/errors/app-error';
import { CategoryService } from './../../../services/category.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-category-edit',
  templateUrl: './category-edit.component.html',
  styleUrls: ['./category-edit.component.css']
})
export class CategoryEditComponent implements OnInit {
  category: any= {};
  category_id: string = "";
  categoryForm: FormGroup;
  errors:any=[];
  constructor(
    private admin_store: AdminStoreService,
    private fb:FormBuilder,
    private messageService: MessageService,
    private category_svc: CategoryService,
    private router:Router,
    vcr:ViewContainerRef,
  ) { 
    // this.toast.setRootViewContainerRef(vcr);
    // this.categoryForm=fb.group({
    //   name:[this.category.name,Validators.required],
    //   description:[this.category.description,Validators.required]
    // })
    categoryForm:FormGroup;
  }

  ngOnInit() {

    this.category = this.admin_store.getItem("category");

    this.categoryForm = new FormGroup({
      'name': new FormControl(this.category.name, Validators.required),
      'description': new FormControl(this.category.description, [Validators.required]),
    })


  }

  editCategory(form:any){
      this.category_svc.editCategory(form, this.category._id).subscribe(res=>{
        this.messageService.add({
          severity: 'success',
          summary: 'Category Updated',
          detail: 'Category updated successfully!'
        });

        // this.router.navigate(['super-admin','category','list']); 
      }, (error: AppError) => {
        this.messageService.add({
          severity: 'error',
          summary: 'Category Update Failed',
          detail: 'Make sure to use valid inputs'
        });
        if (error instanceof BadInputError) {
          console.log(" Not UPdated");        
        }
        
      })
    
  }

  get name() {
    return this.categoryForm.get('name');
  }

  get description() {
    return this.categoryForm.get('description');
  }

}
