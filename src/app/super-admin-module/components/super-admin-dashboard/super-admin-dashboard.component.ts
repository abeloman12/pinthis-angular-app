import { Router } from '@angular/router';
// import { UnauthorizedError, NotFoundError } from './../../../shared/errors/app-error';
import { DashboardService } from './../../services/dashboard.service';
import { Component, OnInit } from '@angular/core';
import * as async from 'async';
@Component({
  selector: 'app-super-admin-dashboard',
  templateUrl: './super-admin-dashboard.component.html',
  styleUrls: ['./super-admin-dashboard.component.css']
})
export class SuperAdminDashboardComponent implements OnInit {
  customerCount = 0
  categoryCount = 0
  postCount = 0
  subcategoryCount = 0
  displayChart = false;

  data: any; 
  constructor(
    private dashboard_svc:DashboardService,
    private router:Router,
  ) { }

  ngOnInit() {
    this.dashboard_svc.getAllCustomers().subscribe(res =>{
      this.customerCount = res.length
    })
    this.dashboard_svc.getAllCategories().subscribe(categories =>{
      this.categoryCount = categories.length
    })

    this.dashboard_svc.getAllPosts().subscribe(posts =>{
      this.postCount = posts.length
    })

    this.dashboard_svc.getAllSubCategories().subscribe(sub =>{
      this.subcategoryCount = sub.length
    })
    this.getYearReport()
  }

  getYearReport() {
    // async.waterfall([
    //   (done) => {
    //     this.dashboard_svc.getNumberOfCustomerPerYear()
    //       .subscribe(response => {
    //         console.log(response);
    //         done(null, response);
    //       }, (error) => {
    //         console.log(error);
    //         done(error);
    //       })
    //   },
    //   (customerDataset, done) => {
    //     this.dashboard_svc.getNumberOfBusinessPerYear()
    //       .subscribe(response => {
    //         console.log(response);
    //         done(null, customerDataset, response);
    //       }, (error) => {
    //         done(error);
    //       })
    //   },
    //   (customerDataset, businessDataset, done) => {
    //     this.dashboard_svc.getNumberOfReceiptPerYear()
    //       .subscribe(response => {
    //         console.log(response);
    //         done(null, customerDataset, businessDataset, response);
    //       }, (error) => {
    //         done(error);
    //       })
    //   },
    //   (customerDataset, businessDataset, receiptDataset, done) => {
    //     this.dashboard_svc.getNumberOfTransactionPerYear()
    //       .subscribe(response => {
    //         console.log("PARENT");
    //         console.log(response);
    //         done(null, customerDataset, businessDataset, receiptDataset, response);
    //       }, (error) => {
    //         done(error);
    //       })
    //   },
    //   (customerDataset, businessDataset, receiptDataset, transactionDataset, done) => {
    //     console.log("Final");
    //     this.data = {
    //       labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    //       datasets: [
    //           {
    //               label: 'Number of Businesses Registered',
    //               data: businessDataset,
    //               fill: false,
    //               borderColor: '#0073b7'
    //           },
    //           {
    //               label: 'Number of Customers Registered',
    //               data: customerDataset,
    //               fill: false,
    //               borderColor: '#00a65a',
    //           },
    //           {
    //               label: 'Number of Receipts Issued',
    //               data: receiptDataset,
    //               fill: false,
    //               borderColor: '#d33724'
    //           },
    //           {
    //             label: 'Number of Transactions',
    //             data: transactionDataset,
    //             fill: false,
    //             borderColor: '#f39c12'
    //           }
    //       ]
    //     }

    //     this.displayChart = true;
    //   }
    // ], (error) => {
    //   throw error;
    //   // if (error instanceof UnauthorizedError) {
    //   //   this.router.navigate(['/login']);
    //   // }
    //   // else if (error instanceof NotFoundError) {
    //   //   this.router.navigate(['/404']);
    //   // }
    //   // else {
    //   //   throw error;
    //   // }
    // })
  }

}
