import { Subject } from 'rxjs';
import { CustomerService } from './../../../services/customer.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {
  private customerList: any[] = [];
  public dtTrigger: Subject<any> = new Subject();
  
  constructor(
    private customer_svc: CustomerService
  ) { }

  ngOnInit() {
    this.customer_svc.getAll()
      .subscribe((response) => {
        this.customerList = response;
        this.dtTrigger.next();
    });
}

  ngOnDestroy() {
    this.dtTrigger.unsubscribe();
  }

}
