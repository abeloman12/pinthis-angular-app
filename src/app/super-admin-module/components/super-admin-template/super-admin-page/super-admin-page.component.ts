import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-super-admin-page',
  templateUrl: './super-admin-page.component.html',
  styleUrls: ['./super-admin-page.component.css']
})
export class SuperAdminPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    setTimeout (() => {
      this.loadScript("page-load.js");
   }, 1000);
  }
  public loadScript(jsFile){
    let body=<HTMLDivElement> document.body;
    let script=document.createElement('script');
    script.src='../../../assets/template-js/'+jsFile;
    script.async=true;
    script.defer=true;
    body.appendChild(script);
  }

}
