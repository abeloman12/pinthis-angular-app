import { DashboardService } from './../../../services/dashboard.service';
import { AdminStoreService } from './../../../../shared/services/admin-store.service';
import { LogoutService } from './../../../../shared/services/logout.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/main/services/auth.service';

@Component({
  selector: 'app-super-admin-header',
  templateUrl: './super-admin-header.component.html',
  styleUrls: ['./super-admin-header.component.css']
})
export class SuperAdminHeaderComponent implements OnInit {
  user;
  admin;
  constructor(
    private admin_store:AdminStoreService,
    private logout_svc: LogoutService,
    private auth_svc: AuthService,
    private dash_svc: DashboardService
  ) { }

  ngOnInit() {
    this.user = this.auth_svc.getCurrentUser();
    console.log(this.user);
    this.dash_svc.getAdmin(this.user._id)
        .subscribe((response) => {
          localStorage.setItem("admin", this.admin);
          this.admin = response;
      });
  }
  logout(){
    console.log("Logging out =-=")
    this.logout_svc.logout()
  }

}
