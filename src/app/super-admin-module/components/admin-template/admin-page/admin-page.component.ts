import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    setTimeout (() => {
     
      this.loadVendorScript("jquery.nicescroll.min.js");
   }, 1000);


  }
  public loadVendorScript(jsFile){
    let body=<HTMLDivElement> document.body;
    let script=document.createElement('script');
    script.src='../../../assets/vendor_js/'+jsFile;
    script.async=true;
    script.defer=true;
    body.appendChild(script);
  }


}
