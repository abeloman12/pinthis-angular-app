import { DashboardService } from './../../../services/dashboard.service';
import { LogoutService } from './../../../../shared/services/logout.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/main/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-navbar',
  templateUrl: './admin-navbar.component.html',
  styleUrls: ['./admin-navbar.component.css']
})
export class AdminNavbarComponent implements OnInit {
  user;
  admin;

  constructor(
    private router: Router,
    public authService: AuthService,
    private logout_svc: LogoutService,
    private dash_svc: DashboardService
  ) { }

  ngOnInit() {
    setTimeout (() => {
      this.loadScript("scripts.js");
      this.loadScript("custom.js");
   }, 1000);

   this.user = this.authService.getCurrentUser();
  //  console.log(this.user);
   this.dash_svc.getAdmin(this.user._id)
       .subscribe((response) => {       
         this.admin = response;
         console.log(this.admin);
         localStorage.setItem("admin", this.admin);
     });

  }
  public loadScript(jsFile){
    let body=<HTMLDivElement> document.body;
    let script=document.createElement('script');
    script.src='../../../assets/js/'+jsFile;
    script.async=true;
    script.defer=true;
    body.appendChild(script);
  }

  logout(){
    this.logout_svc.logout()
  }

}
