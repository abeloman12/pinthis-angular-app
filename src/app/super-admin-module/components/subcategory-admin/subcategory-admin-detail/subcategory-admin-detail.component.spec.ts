import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubcategoryAdminDetailComponent } from './subcategory-admin-detail.component';

describe('SubcategoryAdminDetailComponent', () => {
  let component: SubcategoryAdminDetailComponent;
  let fixture: ComponentFixture<SubcategoryAdminDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubcategoryAdminDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubcategoryAdminDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
