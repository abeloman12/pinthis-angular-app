import { AppError } from 'src/app/shared/errors/app-error';
import { SubadminService } from 'src/app/super-admin-module/services/subadmin.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminStoreService } from './../../../../shared/services/admin-store.service';

@Component({
  selector: 'app-subcategory-admin-detail',
  templateUrl: './subcategory-admin-detail.component.html',
  styleUrls: ['./subcategory-admin-detail.component.css']
})
export class SubcategoryAdminDetailComponent implements OnInit {

  subadmin: any = {};

  constructor(
    private admin_store: AdminStoreService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private messageService: MessageService,
    private subadmin_svc: SubadminService
  ) { }

  ngOnInit() {
    this.subadmin = this.admin_store.getItem("subAdmin");
  }

  setTerminate(subadmin, value) {
    let boolValue = false;
    if (value == 'true') {
      boolValue = true;
    }

    this.subadmin_svc.setTermination(subadmin._id, boolValue)
    .subscribe((response) => {
      subadmin.is_terminated = boolValue;
      this.messageService.add({
        severity: 'success',
        summary: 'Sub administrator status changed',
        detail: 'Sub administrator\'s status has been changed successfully'
      });
      // this.router.navigate(['super-admin','subadmin','list'])
    }, (error: AppError) => {
        this.messageService.add({
          severity: 'error',
          summary: 'Action Failed',
          detail: 'Please try again!'
        });
    });
  }

  verify(subadmin) {
    this.subadmin_svc.verifySubAdmin(subadmin._id)
    
    .subscribe((response) => {
      subadmin.is_verified = true;
      this.subadmin = subadmin;
      this.messageService.add({
        severity: 'success',
        summary: 'Sub administrator Verified successfully',
        detail: 'Sub administrator has been verified successfully!'
      });
      // this.router.navigate(['super-admin','subadmin','list'])
    }, (error: AppError) => {
        this.messageService.add({
          severity: 'error',
          summary: 'Sub administrator Verification failed',
          detail: 'Please try again and make sure to verify sub administrator!'
        });
    });
  }

}
