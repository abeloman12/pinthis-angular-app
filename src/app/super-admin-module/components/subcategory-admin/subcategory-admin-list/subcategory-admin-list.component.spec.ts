import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubcategoryAdminListComponent } from './subcategory-admin-list.component';

describe('SubcategoryAdminListComponent', () => {
  let component: SubcategoryAdminListComponent;
  let fixture: ComponentFixture<SubcategoryAdminListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubcategoryAdminListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubcategoryAdminListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
