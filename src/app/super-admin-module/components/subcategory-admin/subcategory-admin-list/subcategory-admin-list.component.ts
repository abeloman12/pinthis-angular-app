import { MessageService } from 'primeng/components/common/messageservice';
import { AppError } from 'src/app/shared/errors/app-error';
import { Component, OnInit } from '@angular/core';
import { SubadminService } from 'src/app/super-admin-module/services/subadmin.service';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { AdminStoreService } from './../../../../shared/services/admin-store.service';

@Component({
  selector: 'app-subcategory-admin-list',
  templateUrl: './subcategory-admin-list.component.html',
  styleUrls: ['./subcategory-admin-list.component.css']
})
export class SubcategoryAdminListComponent implements OnInit {
  private subadminList: any[] = [];
  public dtTrigger: Subject<any> = new Subject();

  constructor(
    private admin_store: AdminStoreService,
    private subadmin_svc: SubadminService,
    private router:Router,
    private messageService: MessageService
  ) { }

  ngOnInit() {
      this.subadmin_svc.getOnGoing()
        .subscribe((response) => {
          this.subadminList = response;
          this.dtTrigger.next();
      });
  }

  ngOnDestroy() {
    this.dtTrigger.unsubscribe();
  }

  verify(subadmin) {
    this.subadmin_svc.verifySubAdmin(subadmin._id)
    .subscribe((response) => {
      subadmin.is_verified = true;
    }, (error: AppError) => {
        this.messageService.add({
          severity: 'error',
          summary: 'Sub administrator Verification failed',
          detail: 'Please try again and make sure to verify sub administrator!'
        });
    });
  }

  setTerminate(subadmin, value) {
    let boolValue = false;
    if (value == 'true') {
      boolValue = true;
    }

    this.subadmin_svc.setTermination(subadmin._id, boolValue)
    .subscribe((response) => {
      subadmin.is_terminated = boolValue;
    }, (error: AppError) => {
        this.messageService.add({
          severity: 'error',
          summary: 'Action Failed',
          detail: 'Please try again!'
        });
    });
  }

  goToDetail(subadmin){
    this.admin_store.storeItem("subAdmin",subadmin)
    this.router.navigate(['super-admin','subadmin','detail'])
  }

}
