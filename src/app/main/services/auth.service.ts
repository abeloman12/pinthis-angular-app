import { api_url, AdminStoreService } from './../../shared/services/admin-store.service';
import { BaseService } from './../../shared/services/base.service';
import { throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { map, catchError, retry } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { UnauthorizedError, AppError } from 'src/app/shared/errors/app-error';

interface AuthResponse {
  token: string;
  user_type: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private decoder = new JwtHelperService();
  private url = api_url+ "users/login";

  constructor(
    private baseService: BaseService) { }

  public login(credentials: any) {
    return this.baseService.post(this.url, credentials).pipe(map((response:any) => {
      let result = response;

      if(result && result.token && result.user_type=="admin") {
        localStorage.setItem("token", result.token)
        // this.adminstore_service.setUser()
        return result.user_type;
      }
      else {
        return;
      }
    }));
  }

  getCurrentUser() {
    return this.decoder.decodeToken(localStorage.getItem('token'));
  }

  isLoggedIn() {
    let token = localStorage.getItem('token');
    if (token) {
      return true;
    }
    return false;
  }

}
