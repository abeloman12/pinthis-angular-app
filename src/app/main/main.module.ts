import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './components/login/login.component';
import { AuthService } from './services/auth.service';
// import { CompanyRegistrationComponent } from './components/company-registration/company-registration.component';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/components/common/messageservice';
// import { LandingPageComponent } from './components/landing-page/landing-page.component';

@NgModule({
  declarations: [
    LoginComponent,
    // CompanyRegistrationComponent,
    // LandingPageComponent
  ],
  imports: [
    CommonModule,
    ToastModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {path: 'home', redirectTo: ''},
      {path: '', component: LoginComponent},
      {path:'login', component:LoginComponent}
    ])
  ],
  providers: [
    AuthService,
    MessageService
  ]
})
export class MainModule { }
