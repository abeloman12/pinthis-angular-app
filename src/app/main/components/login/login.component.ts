import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppError, UnauthorizedError } from 'src/app/shared/errors/app-error';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';

enum AuthenticationStatus {
  AUTHENTICATION_SUCCESSFUL = 0,
  AUTHENTICATION_FAILED = 1
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public authenticationStatus: AuthenticationStatus;
  public loginForm: FormGroup = new FormGroup({
    email: new FormControl('abeloman123@gmail.com', Validators.required),
    password: new FormControl('12345678', Validators.required)
  });

  constructor(
    private router: Router,
    private authService: AuthService) { }

  ngOnInit() {
  }

  get email() {
    return this.loginForm.get('email');
  }

  get password() {
    return this.loginForm.get('password');
  }

  onSubmit() {
    this.authService.login(this.loginForm.value)
      .subscribe((response) => {
        this.authenticationStatus = AuthenticationStatus.AUTHENTICATION_SUCCESSFUL;

        if (response === 'admin') {
          this.router.navigate(['/super-admin/dashboard']);
        }
        else {
          this.authenticationStatus = AuthenticationStatus.AUTHENTICATION_FAILED;
        }
      }, (error: AppError) => {
        if (error instanceof UnauthorizedError) {
          this.authenticationStatus = AuthenticationStatus.AUTHENTICATION_FAILED;
        }
        else {
          console.log(error);
          throw error;
        }
      })
  }



}
