import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: [
  '../../../../assets/index_modules/mobirise-icons/mobirise-icons.css',
  '../../../../assets/index_modules/tether.min.css',
  '../../../../assets/index_modules/animate.min.css',
  '../../../../assets/index_modules/socicon/css/styles.css',
  '../../../../assets/index_modules/style.css',
  '../../../../assets/index_modules/mbr-additional.css',
  '../../../../assets/css/index.css',
  

]
})
export class LandingPageComponent implements OnInit {
  landingForm:FormGroup;
  constructor(
    private fb:FormBuilder
  ) {
    this.landingForm = this.fb.group({
      a:['']
    })
  }
  
  ngOnInit() {
  }

}
