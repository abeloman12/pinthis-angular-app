import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// import { AdminModule } from './admin/admin.module';
import { MainModule } from './main/main.module';
import { SharedModule } from './shared/shared.module';
import { RouterModule } from '@angular/router';
import { AppErrorHandler } from './shared/errors/app-error-handler';
import { SuperAdminModule } from './super-admin-module/super-admin-module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SuperAdminModule,
    MainModule,
    SharedModule,
    RouterModule.forRoot([])
  ],
  providers: [
    AppErrorHandler,
    { provide: ErrorHandler, useClass: AppErrorHandler }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
