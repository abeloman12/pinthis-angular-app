export class AppError {
  constructor(public originalError?: any) {}
}

export class BadInputError extends AppError {}

export class NotFoundError extends AppError {}

export class UnauthorizedError extends AppError {}
