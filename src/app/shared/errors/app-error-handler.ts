import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { AppError, NotFoundError, UnauthorizedError } from './app-error';

@Injectable()
export class AppErrorHandler implements ErrorHandler {

  constructor(private injector: Injector) {}

  handleError(error: any) {
    let router = this.injector.get(Router);
    if (error.originalError instanceof HttpErrorResponse) {
      console.log(error);
      router.navigate(['/503']);
    }
    else if (error instanceof NotFoundError) {
      router.navigate(['/404']);
    }
    else if (error instanceof UnauthorizedError) {
      router.navigate(['/403']);
    }
    // else {
    //   console.log(error);
    //   router.navigate(['/500']);
    // }
  }
}
