import { AbstractControl, ValidationErrors } from '@angular/forms';

export class PasswordConfirmationValidator {
  static passwordConfirmationValidator(control: AbstractControl): ValidationErrors | null {
    let password: string = control.get('password').value;
    let confirmPassword: string = control.get('confirm_password').value;

    if (password !== confirmPassword) {
      control.get('confirm_password').setErrors({
        passwordMismatch: true
      });
    }

    return null;
  }
}
