import { AbstractControl, ValidationErrors, AsyncValidatorFn } from '@angular/forms';
import { AppError } from '../errors/app-error';
import { throwError } from 'rxjs';
import { ValidatorService } from '../services/validator.service';


export function emailAvailabilityValidator(validatorService: ValidatorService): AsyncValidatorFn {
  return (control: AbstractControl): Promise<ValidationErrors> | null => {
    return new Promise(resolve => {
      validatorService.checkEmailAvailability(control.value).subscribe(
        (response) => {
          if (response) {
            resolve(null);
          }
          else {
            resolve({emailIsNotAvailable: true});
          }
        }, (error: AppError) => {
          return throwError(error);
        });
    });
  }
}
