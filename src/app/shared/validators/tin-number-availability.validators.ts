import { AsyncValidatorFn, ValidationErrors, AbstractControl } from '@angular/forms';
import { AppError } from '../errors/app-error';
import { throwError } from 'rxjs';
import { ValidatorService } from '../services/validator.service';

export function tinnumberAvailabilityValidator(validatorService: ValidatorService): AsyncValidatorFn {
  return (control: AbstractControl): Promise<ValidationErrors> | null => {
    return new Promise(resolve => {
      validatorService.checkTinNumberAvailability(control.value).subscribe(
        (response) => {
          if (response) {
            resolve(null);
          }
          else {
            resolve({tinNumberIsNotAvailable: true});
          }
        }, (error: AppError) => {
          return throwError(error);
        });
    });
  }
}
