import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { sharedInjectable } from './services/shared.injectable';
import { ValidatorService } from './services/validator.service';
import { NotFoundComponent } from './components/errors/not-found/not-found.component';
import { InternalServerErrorComponent } from './components/errors/internal-server-error/internal-server-error.component';
import { ForbiddenComponent } from './components/errors/forbidden/forbidden.component';
import { RouterModule } from '@angular/router';
import { ServiceUnavailableComponent } from './components/errors/service-unavailable/service-unavailable.component';

@NgModule({
  declarations: [
    NotFoundComponent,
    InternalServerErrorComponent,
    ForbiddenComponent,
    ServiceUnavailableComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '403',
        component: ForbiddenComponent
      },
      {
        path: '404',
        component: NotFoundComponent
      },
      {
        path: '500',
        component: InternalServerErrorComponent
      },
      {
        path: '503',
        component: ServiceUnavailableComponent
      }
    ])
  ],
  providers : [
    sharedInjectable,
    ValidatorService
  ]
})
export class SharedModule { }

