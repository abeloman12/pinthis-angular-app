import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
export const base_url = 'http://localhost:3000/v1/';
export const api_url = 'http://localhost:3000/v1/';
export const current_year = '2019';
import { FormGroup } from '@angular/forms';

// custom validator to check that two fields match
export function MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];

        if (matchingControl.errors && !matchingControl.errors.mustMatch) {
            // return if another validator has already found an error on the matchingControl
            return;
        }

        // set error on matchingControl if validation fails
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
}
@Injectable()
export class AdminStoreService {

  constructor() { }
  setUser(value:any):Boolean{
    localStorage.setItem('admin',JSON.stringify(value));
    return true;
  }
  deleteUser(){
    localStorage.removeItem('admin');
  }
  getUser():any{
    return JSON.parse(localStorage.getItem('admin'));
  }
  storeItem(key:string,value:any){
    localStorage.setItem(key,JSON.stringify(value));
    return true;
  }
  getItem(key:string){
    if(JSON.parse(localStorage.getItem(key))!=undefined){
      return JSON.parse(localStorage.getItem(key));
    }else{
      return [];
    }
    
  }
  deleteItem(item){
    localStorage.removeItem(item);
  }
}
