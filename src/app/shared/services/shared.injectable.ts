import { BaseService } from './base.service';
import { LogoutService } from './logout.service';
import { AdminStoreService } from './admin-store.service';

export const sharedInjectable: Array<any> = [
    {provide:AdminStoreService,useClass:AdminStoreService},
    {provide:LogoutService,useClass:LogoutService},
    {provide:BaseService,useClass:BaseService}
];