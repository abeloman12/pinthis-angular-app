import { AppError } from 'src/app/shared/errors/app-error';
import { UnauthorizedError } from './../errors/app-error';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map, retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  constructor(private http:HttpClient) { 
    
  }

  get(url: string, options?: any): Observable<any>{
    if(options){
      return this.http.get(url, options).pipe(map((response:any) => {
        return response;
      }), retry(1), catchError(this.handleError))
    }
    else{
      return this.http.get(url).pipe(map((response:any) => {
        return response;
      }), retry(1), catchError(this.handleError))
    }
  }

  delete(url: string, options?: any): Observable<any>{
    if(options){
      return this.http.delete(url, options).pipe(map((response:any) => {
        return response;
      }), retry(1), catchError(this.handleError))
    }
    else{
      return this.http.delete(url).pipe(map((response:any) => {
        return response;
      }), retry(1), catchError(this.handleError))
    }
  }

  post(url: string, postBody: any, options?: any): Observable<any>{
    if(options){
      return this.http.post(url, postBody, options).pipe(map((response:any) => {
        return response;
      }), retry(1), catchError(this.handleError))
    }
    else{
      return this.http.post(url, postBody).pipe(map((response:any) => {
        return response;
      }), retry(1), catchError(this.handleError))
    }
  }

  put(url: string, putBody: any, options?: any): Observable<any>{
    if(options){
      console.log(options);
      return this.http.put(url, putBody, options).pipe(map((response:any) => {
        return response;
      }), retry(1), catchError(this.handleError))
    }
    else{
      return this.http.put(url, putBody).pipe(map((response:any) => {
        return response;
      }), retry(1), catchError(this.handleError))
    }
  }

  prepareRequestHeader() {
    let token = localStorage.getItem('token');
    return {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token
      })
    };
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status == 401) {
      return throwError(new UnauthorizedError(error));
    }
    return throwError(new AppError(error));
  }
}
