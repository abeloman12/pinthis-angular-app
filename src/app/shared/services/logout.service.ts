import { AdminStoreService } from './admin-store.service';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class LogoutService {

  constructor(private router:Router,private admin_store:AdminStoreService) { }
  logout(){
    this.admin_store.deleteUser();
    localStorage.removeItem("token");
    this.router.navigate(['login']);
  }
}
