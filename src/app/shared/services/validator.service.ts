import { throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map, catchError, retry } from 'rxjs/operators';
import { AppError } from '../errors/app-error';

interface ValidationResponse {
  isAvailable: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class ValidatorService {

  constructor(private httpClient: HttpClient) { }

  checkTinNumberAvailability(vendor_tin_number: string) {
    return this.httpClient.post('http://localhost:3000/v1/vendors/checkTinNumberIsUnique', {vendor_tin_number: vendor_tin_number}).pipe(
      map((response: ValidationResponse) => {
        return response.isAvailable;
      }),
      retry(1),
      catchError(this.handleError)
    );
  }

  checkEmailAvailability(email: string) {
    return this.httpClient.post('http://localhost:3000/v1/users/checkEmailIsUnique', {email: email}).pipe(
      map((response: ValidationResponse) => {
        return response.isAvailable;
      }),
      retry(1),
      catchError(this.handleError)
    );
  }


  private handleError(error: HttpErrorResponse) {
    return throwError(new AppError(error));
  }
}
